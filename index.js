/*alert("test")*/

function displayMsgToSelf (){
	console.log("Helllo Charles of 2022")
};

displayMsgToSelf();
/*displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();*/

let count = 10;
while (count !== 0){
	displayMsgToSelf();
	count--;
}

/*
While loop allow us to repeat an instruction as long as the condition is true

while (condition){
	instruction
	increment/decrement
}

*/
let number = 5;
while (number !== 0){
	//the current value will be printed out
	console.log("While" + " " + number);
	//decreases by 1
	number--;
};

/*while (number !== 10){
	console.log("While" + " " + number);
	number++;
};
*/
//Do while loop

/*
Number function works like a parseInt it changes the string type to number type
*/

/*let number2 = Number(prompt("Give me a number"));
do {
	console.log(`Do while ${number2}`);
	number2 +=1;

} while (number2 < 10)*/

/*let counter = 1;
do {
	console.log(counter);
	counter++;
} while (counter <= 21);*/

//For Loop - more flexible than while and do while loop

/*for ( let count = 0; count <= 20; count++){
	console.log(count)
};*/

let myString = "alex";
console.log(myString.length);

/*
strings are special data types since they have access to functions and other pieces of information 

*/

console.log(myString[0]);
console.log(myString[3]);

for (let x = 0; x < myString.length; x++ ){
	console.log(myString[x])
};

let myName = "Charles"

for (let i = 0;  i < myName.length; i++){
	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){
		console.log(3)
	} else{
		console.log(myName[i])
	}

};

for (let count = 0; count <=20; count++){
		if (count % 2 === 0) {
			continue;
		}
		console.log(`Continue and break ${count}`)
		if (count > 10) {
			break;
		}
	}

let name = "alexandro";
for (let i = 0; i < name.length; i++){
	console.log(name[i]);
	if(name[i].toLowerCase()=== "a"){
		console.log("Continue to the next iteration")
	}
	if (name[i] == "d"){
		break;
	}
}





