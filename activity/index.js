let number = Number(prompt("Give me a number"));
console.log(`The number you provided is ${number}`);
for ( number ; number >= 0; number--){
	if (number % 10 === 0 && number > 50){
		console.log("The number is divisible by 10 and will be skipped.")
		continue;
	}
	if (number % 5 === 0 && number > 50){
		console.log(`${number}`)
		continue;
	}
	if ( number <= 50 ){
		console.log("The current value is at 50. Terminating the loop")
		break;
	}
	
};